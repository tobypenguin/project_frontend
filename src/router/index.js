import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/manageuser',
    name: 'Manageuser',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ManageUser/UserTable.vue')
  },
  {
    path: '/institution',
    name: 'institution',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/ManageInstitution/InstitutionTable.vue')
  },
  {
    path: '/listroom',
    name: 'ListRoom',
    component: () => import(/* webpackChunkName: "about" */ '../views/ListRoom.vue')
  },
  {
    path: '/booking',
    name: 'Booking',
    component: () => import(/* webpackChunkName: "about" */ '../views/Booking.vue')
  },
  {
    path: '/approve',
    name: 'Approve',
    component: () => import(/* webpackChunkName: "about" */ '../views/Approve.vue')
  },
  {
    path: '/building',
    name: 'Building',
    component: () => import(/* webpackChunkName: "about" */ '../views/ManageBuilding/BuildingTable.vue')
  },
  {
    path: '/room',
    name: 'Room',
    component: () => import(/* webpackChunkName: "about" */ '../views/ManageRoom/RoomTable.vue')
  },
  {
    path: '/approver',
    name: 'Approver',
    component: () => import(/* webpackChunkName: "about" */ '../views/ManageApprover/approverTable.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
