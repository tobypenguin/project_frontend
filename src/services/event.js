import api from './api'

export function getEvents (startDate, endDate) {
  return api.get('/bookings', { params: { startDate: startDate, endDate: endDate } })
}
